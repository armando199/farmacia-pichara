import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//Rutas

import { APP_ROUTING } from './app.routes';

//Servicios

import { BellezaService} from './service/belleza.service';
import { FarmaceuticoService } from './service/farmaceutico.service';


import { AppComponent } from './app.component';
import { ClienteComponent } from './components/cliente/cliente.component';
import { CajeroComponent } from './components/cajero/cajero.component';
import { PublicoComponent } from './components/publico/publico.component';


@NgModule({
  declarations: [
    AppComponent,
    ClienteComponent,
    CajeroComponent,
    PublicoComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [
    BellezaService,
    FarmaceuticoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
