import { RouterModule, Routes } from '@angular/router';
import { CajeroComponent } from './components/cajero/cajero.component';
import { ClienteComponent } from './components/cliente/cliente.component';
import { PublicoComponent} from './components/publico/publico.component';


const APP_ROUTES: Routes = [
    { path: 'cajero', component: CajeroComponent },
    { path: 'cliente', component: ClienteComponent },
    { path: 'publico', component: PublicoComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'cliente' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);