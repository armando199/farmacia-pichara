import { Component, OnInit } from '@angular/core';
import { BellezaService } from '../../service/belleza.service';
import { FarmaceuticoService } from '../../service/farmaceutico.service';
import { Belleza } from '../../service/belleza.service';

@Component({
  selector: 'app-cajero',
  templateUrl: './cajero.component.html',
  styleUrls: ['./cajero.component.css'],
  providers:[BellezaService,
    FarmaceuticoService
  ]
})
export class CajeroComponent implements OnInit {

  b: Belleza[];
  f: any = [];

  //Variable para mostrar fecha actual en el componente
  fecha = new Date();
  
  //Variable para contador de belleza, se utiliza en el html
  bellezas: any;
  //Variable para contador de farmacia, se utiliza en el html
  farmacia: any;


  //Contador de belleza
  contador = 0;
  //Contador de farmacia
  contador2 = 0;


  //Variables para colocar visibles e invisibles partes del componente

  visible = false;
  visible2 = false;

  constructor(private farmaceutico: FarmaceuticoService,
              private belleza: BellezaService
    ) {

     }

  ngOnInit() {

    this.b = this.belleza.belleza;

    this.f = this.farmaceutico.farmaceutico;
  }


  //Funcion para realizar el contador de belleza,
  // Y para mostrar o quitar pantalla

  mostrarBelleza(){

    this.contador++;

    this.bellezas = this.contador;
    
    this.visible = true ;
    this.visible2 = false ;

    window.localStorage.setItem('belleza', String(this.contador));

  }

  //Funcion para realizar el contador de farmacia,
  // Y para mostrar o quitar pantalla

  mostrarFarmacia(){

    this.contador2++;

    this.farmacia = this.contador2;

    this.visible2 = true ;
    this.visible = false ;

    window.localStorage.setItem('farmacia', String(this.contador2));

  }
  

}
