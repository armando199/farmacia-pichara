import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  //Titulo 1 y 2 de la Farmacia
  title = 'Farmacia ';
  second = 'Pichara';

  //Variables para asignar nombre a las tarjetas
  belleza = 'Productos de Belleza';
  farmaceutico = 'Productos Farmaceuticos';
  
  //Variable para contador de belleza, se utiliza en el html
  bellezas: any;
  //Variable para contador de farmacia, se utiliza en el html
  farmacia: any;
  
  //Contador de belleza
  contador = 0;
  //Contador de farmacia
  contador2 = 0;

  //Variables para colocar visibles e invisibles partes del componente

  visible = false;
  visible2 = false;

  //Variable para mostrar fecha actual en el componente

  fecha = new Date();



  constructor() { }

  ngOnInit() {
  }

  //Funcion contador para dar un numero en belleza a un cliente
  //Y para ocultar la caja una vez se presione el boton de farmacia
  productoBelleza(){

    this.visible = true ;
    this.visible2 = false ;
    

    this.contador++;

    this.bellezas = this.contador;

  }

  //Funcion contador para dar un numero en farmacia a un cliente
  //Y para ocultar la caja una vez se presione el boton de belleza

  productoFarmacia(){
    
    this.visible = false ;
    this.visible2 = true ;
    
    this.contador2++;

    this.farmacia = this.contador2;
  }

}