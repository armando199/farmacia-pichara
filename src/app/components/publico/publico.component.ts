import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-publico',
  templateUrl: './publico.component.html',
  styleUrls: ['./publico.component.css']
})
export class PublicoComponent implements OnInit {

  //Variable para la fecha
  fecha = new Date();

  farm: string = "";
  bell: string = "";

  

  constructor() { }

  ngOnInit() {
    
    this.obtenerContadorBelleza();
    this.obtenerContadorFarmacia();

  }

  obtenerContadorBelleza(){

    this.bell = localStorage.getItem('belleza');

  }

  obtenerContadorFarmacia(){
    
    this.farm = localStorage.getItem('farmacia');

  }

  

}
