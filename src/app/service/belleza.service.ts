import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BellezaService {

  public belleza:Belleza[] = [
    {
      nombre: "Valmy",
      funcion: "Esmalte para uñas",
      medida: "Pequeño",
      precio:"2$"
    }
    
  ];

  constructor() { }

}

export interface Belleza{
  nombre: string;
  funcion: string;
  medida: string;
  precio: string;
};