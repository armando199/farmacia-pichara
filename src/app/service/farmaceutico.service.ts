import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FarmaceuticoService {

  public farmaceutico:Farmaceutico[] = [
    {
      nombre: "Atamel",
      funcion: "Analgesico contra dolores generales",
      medida: "Caja mediana",
      precio:"5$"
    }
    
    
  ];

  constructor() { }
}

export interface Farmaceutico{
  nombre: string;
  funcion: string
  medida: string;
  precio: string;
};